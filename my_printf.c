#include <stdio.h>
#include <stdarg.h>


int ft_printf(const char* fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    int r = vprintf(fmt, ap);
    fflush(stdout);
    va_end(ap);
    return r;
}



int ft_printf2(const char* fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    int r = vprintf(fmt, ap);
    printf(" SOSI HUY SHKOLA21");
    fflush(stdout);
    va_end(ap);                                                                                                           
    return r;
}

int ft_printf3(const char* fmt, ...) {
    int r = printf("%s", fmt);
    fflush(stdout);
    return r;
}