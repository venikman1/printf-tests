#!/bin/bash
if [[ "$1" == "" || "$2" == "" ]];
then
    echo "Usage: $0 <library file> <printf func name>"
    exit 1
fi;

gcc -Wformat=0 mf.c main.c $1 -D custom_printf=$2 -lrt -o tests-binary
./tests-binary > tests-result.txt 
grep "\[ERROR\]" --color=auto -A 2 -B 1 tests-result.txt
grep "\[STATUS\]" --color=auto -A 2 tests-result.txt
rm tests-binary
