#ifndef MF_H
#define MF_H

#define _GNU_SOURCE

#include <sys/mman.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern const char* SHM_PATH;

struct MappedMemory {
    void* address;
    size_t length;
};

int write_all(int fd, const void* s, size_t len);
int create_memory_file();
off_t file_size(int fd);
struct MappedMemory map_memory_file(int fd);
void unmap_memory_file(struct MappedMemory mm);
void exit_with_error(const char* str);

#endif
