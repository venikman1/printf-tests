
#include "mf.h"

#include <string.h>
#include <stdlib.h>
#include <limits.h>

#ifndef custom_printf
#define custom_printf ft_printf
#endif

int custom_printf(const char*, ...);

struct PrintfResult {
    int returned;
    const char* printed;
    size_t size;
};

char* escape_output(const char* output, int size) {
    static const char esc_char[]= { '\a','\b','\f','\n','\r','\t','\v','\\','\0'};
    static const char essc_str[]= {  'a', 'b', 'f', 'n', 'r', 't', 'v','\\', '0'};
    char* escaped;
    if (!(escaped = malloc(size * 2 + 1)))
        exit_with_error("malloc");
        
    int escaped_size = 0;
    for (int i = 0; i < size; ++i) {
        char esc_done = 0;
        for (int esc_idx = 0; esc_idx < sizeof(esc_char); ++esc_idx) {
            if (output[i] == esc_char[esc_idx]) {
                escaped[escaped_size++] = '\\';
                escaped[escaped_size++] = essc_str[esc_idx];
                esc_done = 1;
                break;
            }
        }
        if (!esc_done)
            escaped[escaped_size++] = output[i];
    }
    escaped[escaped_size++] = 0;
    return escaped;
}

int diff_printf_output(struct PrintfResult right_answer, struct PrintfResult test_answer) {
    char* esc_right = escape_output(right_answer.printed, right_answer.size);
    char* esc_test = escape_output(test_answer.printed, test_answer.size);
    if (right_answer.size != test_answer.size || strncmp(right_answer.printed, test_answer.printed, right_answer.size) != 0) {
        printf("[ERROR] Wrong output:\n"
               "Expected %10ld bytes: \"%s\"\n"
               "Got      %10ld bytes: \"%s\"\n",
               right_answer.size, esc_right,
               test_answer.size, esc_test);
        return 1;
    }
    if (right_answer.returned != test_answer.returned) {
        printf("[ERROR] Wrong return of function: expected %d, but got %d\n", right_answer.returned, test_answer.returned);
        return 1;
    }
    free(esc_right);
    free(esc_test);
    printf("OK\n");
    return 0;
}

#define TEST_CASE(...) \
{ \
    struct PrintfResult right_pr, test_pr; \
    int right_mf = create_memory_file(); \
    int test_mf = create_memory_file(); \
    int stdout_backup; \
    if ((stdout_backup = dup(1)) == -1) \
        exit_with_error("dup stdout"); \
    \
    if (dup2(right_mf, 1) == -1) \
        exit_with_error("dup2 right mem file to stdout"); \
    \
    right_pr.returned = printf(__VA_ARGS__); \
    fflush(stdout); \
    \
    if (dup2(test_mf, 1) == -1) \
        exit_with_error("dup2 test mem file to stdout"); \
    test_pr.returned = custom_printf(__VA_ARGS__); \
    fflush(stdout); \
    \
    if (dup2(stdout_backup, 1) == -1) \
        exit_with_error("dup2 backup stdout to real stdout"); \
    close(stdout_backup); \
    \
    struct MappedMemory right_mm = map_memory_file(right_mf), test_mm = map_memory_file(test_mf); \
    right_pr.printed = right_mm.address; right_pr.size = right_mm.length; \
    test_pr.printed = test_mm.address; test_pr.size = test_mm.length; \
    \
    printf("Test case %s\n", #__VA_ARGS__); \
    ok_tests += 1 - diff_printf_output(right_pr, test_pr); \
    all_tests += 1; \
    printf("\n"); \
    fflush(stdout); \
    unmap_memory_file(right_mm); \
    unmap_memory_file(test_mm); \
    close(right_mf); \
    close(test_mf); \
}

#define INIT_TESTS() \
    int all_tests = 0; \
    int ok_tests = 0;

#define PRINT_STATUS() \
{ \
    printf("[STATUS]\nPassed: %4d\nFailed: %4d\n", ok_tests, all_tests - ok_tests); \
    fflush(stdout); \
}

int main() {
    INIT_TESTS()

    /* Simple conversions */
    TEST_CASE("%d\n", 4242);
    TEST_CASE("%i\n", 4242);
    TEST_CASE("%u\n", 4242);
    TEST_CASE("%x\n", 4242);
    TEST_CASE("%X\n", 4242);
    TEST_CASE("%c\n", 'Y');
    TEST_CASE("%s\n", "School 42");
    TEST_CASE("%p\n", "Meaningless string");
    TEST_CASE("%%\n");

    /* Flag - */
    TEST_CASE("$%-21d$\n", 4242);
    TEST_CASE("$%-21i$\n", 4242);
    TEST_CASE("$%-21u$\n", 4242);
    TEST_CASE("$%-21x$\n", 4242);
    TEST_CASE("$%-21X$\n", 4242);
    TEST_CASE("$%-21c$\n", 'Y');
    TEST_CASE("$%-21s$\n", "School 42");
    TEST_CASE("$%-21p$\n", "Meaningless string");

    /* Flag 0 */
    TEST_CASE("$%021d$\n", 4242);
    TEST_CASE("$%021i$\n", 4242);
    TEST_CASE("$%021u$\n", 4242);
    TEST_CASE("$%021x$\n", 4242);
    TEST_CASE("$%021X$\n", 4242);
    TEST_CASE("$%021p$\n", "Meaningless string");

    /* Precision */
    TEST_CASE("$%20.10d$\n", 4242);
    TEST_CASE("$%20.10i$\n", 4242);
    TEST_CASE("$%20.10u$\n", 4242);
    TEST_CASE("$%20.10x$\n", 4242);
    TEST_CASE("$%20.10X$\n", 4242);
    TEST_CASE("$%20.10p$\n", "Meaningless string");
    TEST_CASE("$%20.10s$\n", "Super long Super long Super long Super long");

    /* Precision with * */
    TEST_CASE("$%*.*d$\n", 20, 10, 4242);
    TEST_CASE("$%*.*i$\n", 20, 10, 4242);
    TEST_CASE("$%*.*u$\n", 20, 10, 4242);
    TEST_CASE("$%*.*x$\n", 20, 10, 4242);
    TEST_CASE("$%*.*X$\n", 20, 10, 4242);
    TEST_CASE("$%*.*p$\n", 20, 10, "Meaningless string");
    TEST_CASE("$%*.*s$\n", 20, 10, "Super long Super long Super long Super long");

    /* OVERFLOW */
    TEST_CASE("%d\n", INT_MAX);
    TEST_CASE("%i\n", INT_MIN);
    TEST_CASE("%u\n", UINT_MAX);
    TEST_CASE("%x\n", UINT_MAX);
    TEST_CASE("%X\n", UINT_MAX);

    /* 0 byte */
    TEST_CASE("Test %c Test\n", '\0');

    /* Precision tricky cases */
    TEST_CASE("$%5.0d$\n", 0);
    TEST_CASE("$%5.d$\n", 0);
    TEST_CASE("$%5.-2d$\n", 0);
    TEST_CASE("$%5.*d$\n", 0, 0);
    TEST_CASE("$%5.*d$\n", -2, 0);

    /* Precision work with string */
    TEST_CASE("$%.10s$\n", "l");
    TEST_CASE("$%.2s$\n", "<3 Kawaiii");
    TEST_CASE("$%.0s$\n", "<3 Kawaiii");

    /* Multiple flags */
    TEST_CASE("%0023d\n", 42)

    /* Precision with negative numbers */
    TEST_CASE("%10.0d\n", -42)
    TEST_CASE("%10.1d\n", -42)
    TEST_CASE("%10.2d\n", -42)
    TEST_CASE("%10.3d\n", -42)
    TEST_CASE("%10.5d\n", -42)
    TEST_CASE("%10.100d\n", -42)

    /* Precision is greater than width */
    TEST_CASE("$%20.100d$\n", 4242);
    TEST_CASE("$%20.100i$\n", 4242);
    TEST_CASE("$%20.100u$\n", 4242);
    TEST_CASE("$%20.100x$\n", 4242);
    TEST_CASE("$%20.100X$\n", 4242);
    TEST_CASE("$%20.100p$\n", "Meaningless string");
    TEST_CASE("$%20.100s$\n", "Super long Super long Super long Super long");





    PRINT_STATUS()

    // int fd = create_memory_file(); 
    // int stdout_backup;
    // if ((stdout_backup = dup(1)) == -1)
    //     exit_with_error("dup stdout");
    
    // if (dup2(fd, 1) == -1)
    //     exit_with_error("dup2 mem file to stdout");
    
    // printf("Holo4ka is the best waifu!%6.4d", -45);
    // fflush(stdout);

    // if (dup2(stdout_backup, 1) == -1)
    //     exit_with_error("dup2 backup stdout to real stdout");
    
    // close(stdout_backup);

    // struct MappedMemory mm = map_memory_file(fd);
    // printf("Data contained in memory: %.*s\n", (int) file_size(fd), (const char*) mm.address);
    
    // diff_printf_output(M("pidoras", kaka, paka));
    // unmap_memory_file(mm);
    // close(fd);
    return 0;
}