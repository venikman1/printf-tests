#define _GNU_SOURCE

#include "mf.h"


int write_all(int fd, const void* s, size_t len) {
    const char* char_ptr = s;
    while (len > 0) {
        int w = write(fd, char_ptr, len);
        if (w == -1)
            return -1;
        len -= w;
        char_ptr += w;
    }
    return 0;
}

const char* SHM_PATH = "ft_printf_tests_shm";

int create_memory_file() {
    int fd = shm_open(SHM_PATH, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }
    shm_unlink(SHM_PATH);
    return fd;
}

off_t file_size(int fd) {
    struct stat sb;
    if (fstat(fd, &sb) == -1) {
        perror("fstat");
        exit(EXIT_FAILURE);
    }
    return sb.st_size;
}

void exit_with_error(const char* str) {
    perror(str);
    exit(EXIT_FAILURE);
}

struct MappedMemory map_memory_file(int fd) {
    struct MappedMemory mm;
    mm.length = file_size(fd);
    if (!mm.length) {
        mm.address = NULL;
        return mm;
    }
    mm.address = mmap(NULL, mm.length, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    if (mm.address == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }
    return mm;
}

void unmap_memory_file(struct MappedMemory mm) {
    munmap(mm.address, mm.length);
}

int test() {
    size_t mem_size = 256;
    const char* shm_path = "/shared-shit";
    
    int fd = create_memory_file();
    
    // if (ftruncate(fd, mem_size) == -1) {
    //     perror("ftruncate");
    //     exit(EXIT_FAILURE);
    // }

    char s[] = "Holo4ka is the best waifu";
    
    if (write_all(fd, s, sizeof(s)) == -1) {
        perror("write_all");
        exit(EXIT_FAILURE);
    }
    
    

    void* memory = mmap(NULL, mem_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
    
    if (memory == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    printf("Data from mapped memory: %s\n", (char*) memory);

    munmap(memory, mem_size);

    printf("Memory file size: %lld bytes\n", (long long) file_size(fd));
    close(fd);

    exit(EXIT_SUCCESS);
}